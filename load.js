/**
 * Created by Sean McKay on 09/04/2014.
 */

var load_state = {

    preload: function() {
        this.game.stage.backgroundColor = '#AAAAAA';
        this.game.load.image('board', 'assets/BattleshipSea.png');
        this.game.load.image('case', 'assets/BattleshipCase.png');
        this.game.load.image('radar', 'assets/BattleshipRadar.png');
        this.game.load.image('logo', 'assets/Good Gravy.png');
        this.game.load.image('boat2', 'assets/Gunboat.png');
        this.game.load.image('boat3-1', 'assets/Submarine.png');
        this.game.load.image('boat3-2', 'assets/Destroyer.png');
        this.game.load.image('boat4', 'assets/Cruiser.png');
        this.game.load.image('boat5', 'assets/Carrier.png');
        this.game.load.image('hita-1', 'assets/HitA1.png');
        this.game.load.image('hita-2', 'assets/HitA2.png');
        this.game.load.image('hita-3', 'assets/HitA3.png');
        this.game.load.image('hitb', 'assets/HitB.png');
        this.game.load.image('missa', 'assets/MissA.png');
        this.game.load.image('missb', 'assets/MissB.png');
        this.game.load.image('shot', 'assets/Shot.png');
        this.game.load.image('targ', 'assets/Targ.png');
        this.game.load.image('LobbyPhoto', 'assets/LobbyPhoto.png');
        this.game.load.audio('gameMusic', 'assets/Kevin MacLeod - Constancy Part Three.ogg');
        this.game.load.audio('lobbyMusic', 'assets/Kevin MacLeod - Take a Chance.ogg');
        this.game.load.audio('shotAudio', 'assets/qubodup - Excalibur Howitzer Shot.wav');
        this.game.load.audio('hitAudio', 'assets/Shot.wav');
        this.game.load.audio('missAudio', 'assets/FreqMan - Splash 1.wav');
        this.game.load.audio('sinkAudio', 'assets/zimbot - RedAlert Klaxon STTOS Recreated x3.wav');
    },

    create: function(){
        this.textbox = this.game.add.graphics(0,0);
        this.textbox.lineStyle(2, 0x000000, 1);
        this.textbox.beginFill(0x555555, 1);
        this.textbox.drawRect(0, 0, 620, 820);
        this.textbox.position.x = -10;
        this.textbox.position.y = -10;

        this.logo = this.game.add.sprite(0, 200, 'logo');

        this.start = this.game.time.time;
    },

    update: function() {
        var time = game.time.time - this.start;
        if(this.cache.isSoundReady('lobbyMusic') && this.cache.isSoundReady('gameMusic') && time > 3000) {
            this.game.state.start('lobby');
        }
        if(time > 2000){
            this.logo.alpha = 1 - ((time - 2000)/1000);
        }
    }

};