/**
 * Created with JetBrains WebStorm.
 * User: b00225409
 * Date: 01/05/14
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */

var SERVICE_URL = "http://ggbattleships.appspot.com";

/*
{
var board = [
    {"gun": "A12"},
    {"sub": "B13"},
    {"des": "C13"},
    {"cru": "D14"},
    {"car": "E15"}
]
}

var user = "Keramon";
*/

connectionError = function(){
    var style = { font: "20px Arial", fill: "#CC3333" };
    game.add.text(6, 6, "Error: Could not connect to the server.", style);
};

ajaxCall = function(cbNoError, cbFailed, url){
    $.ajax({
        type: "GET",
        url: url,
        async: true,
        contentType: "application/json",
        dataType: "jsonp",
        success: cbNoError,
        error: cbFailed,
        timeout: 6000
    });
};

// Works. Functions as the name describes; returns true if the person is logged into their Google account.
// Success used for transition between lobby and game states.
// For this reason, when in the game state we can assume that anyone in a game is also signed in.
checkLoginStatus = function(cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);
    // default parameter functionality pinched from:
    // http://stackoverflow.com/questions/894860

    var url = SERVICE_URL + "/enter";
    ajaxCall(cbNoError, cbFailed, url);
};

// Used to notify the server that you have dropped out of the match.
// Don't be the douche that cancels matches, 'cause you're opponent will still get the win!
dropOut = function(cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);

    var url = SERVICE_URL + "/leave";
    ajaxCall(cbNoError, cbFailed, url);
};

closingWindow = function(cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);

    var url = SERVICE_URL + "/exit";
    ajaxCall(cbNoError, cbFailed, url);
};

// This works. Grabs a list of the first 10 players on the server.
getPlayers = function(cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);

    var url = SERVICE_URL + "/show";
    ajaxCall(cbNoError, cbFailed, url);
};

// Works, I think. Have to check the datastore.
postLayout = function(ships, cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);

    var url = SERVICE_URL + "/setup" +
        "?g=" + ships[0].getHitbox() +
        "&s=" + ships[1].getHitbox() +
        "&d=" + ships[2].getHitbox() +
        "&c=" + ships[3].getHitbox() +
        "&a=" + ships[4].getHitbox();
    ajaxCall(cbNoError, cbFailed, url);
};

// Not yet implemented. Supposed to grab a specific player's ship layout.
// Realistically, this shouldn't be done, since it allows cheating.
// However, I'm not confident enough in my python to run the game entirely server-side.
getLayout = function(user, cbNoError, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);

    var url = SERVICE_URL + "/show" +
        "?who=" + user;
    ajaxCall(cbNoError, cbFailed, url);
};

findPlayer = function(cbNoError, shots, cbFailed){
    cbFailed = (typeof cbFailed !== 'undefined')?(cbFailed):(connectionError);
    shots = (typeof shots !== 'undefined')?(shots):(false);

    var url = SERVICE_URL + "/get";
    if(shots){
        url += "?s=";
        shots.sort();
        for(var i = 0; i < shots.length && i < 5; i++){
            url += shots[i];
        }
    }
    ajaxCall(cbNoError, cbFailed, url);
};