#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import datetime
from webapp2_extras import json
from google.appengine.ext import ndb
from google.appengine.api import users

DEFAULT_KEY = 'default'

def getLobbyKey(title = DEFAULT_KEY):
    return ndb.Key('Lobby', title)

# An object containing a user ID and layout info for one player
# Having all volatile player data in an object dedicated to that player
# helps ensure that writes will not happen too quickly
class Player(ndb.Model):
    ID = ndb.UserProperty()
    currentChallenge = ndb.UserProperty()
    imBoss = ndb.BooleanProperty()
    gunboat = ndb.StringProperty()
    submari = ndb.StringProperty()
    destroy = ndb.StringProperty()
    cruiser = ndb.StringProperty()
    carrier = ndb.StringProperty()
    lastAttack = ndb.StringProperty(default = "")
    onTurn = ndb.IntegerProperty(default = 0) # So players to not fall behind
    lastOn = ndb.DateTimeProperty(auto_now_add = True)
    def retrieve(self):
        message = {
            "enemy": self.currentChallenge.nickname(),
            "gunboat": self.gunboat,
            "submari": self.submari,
            "destroy": self.destroy,
            "cruiser": self.cruiser,
            "carrier": self.carrier,
            }
        return json.encode(message)

# If the user is logged into their Google account:
#   Add them to the database if they're new, or
#   Retrieve their profile if they're not
# If they are not logged in, prompt a login.
#### Scratch that. The prompting functionality is now handled by the client.
#### Use AmISignedIn instead.
class SignIn(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            newPlayer = Player.query(Player.ID == users.get_current_user()).get()
            if newPlayer:
                now = datetime.datetime.utcnow()
                dT = datetime.timedelta(seconds = (now - newPlayer.lastOn).seconds)
                self.response.write('Welcome back, ' + newPlayer.ID.nickname() +
                                    '! Time elapsed since we last heard from you: ' + str(dT))
                if(dT > datetime.timedelta(days = 10)):
                    self.response.write(' Warning! Your account will be deleted soon!')
                else:
                    self.response.write(' Your account is safe from deletion right now.')
            else:
                newPlayer = Player(ID = users.get_current_user())
                newPlayer.put()
                self.response.write('Oh hey, I made you a new account!')
        else:
            self.response.write('You don\'t seem to be logged in...\nThis game requires a Google account to play. Please log in to your Google account.')

# If the user is signed in, check that they have a profile.
# If they don't, make one.
class AmISignedIn(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        if users.get_current_user():
            newPlayer = Player.query(
                Player.ID == users.get_current_user()
                ).get()
            if newPlayer:
                pass
            else:
                newPlayer = Player(ID = users.get_current_user())
                newPlayer.put()
            self.response.write(callback + '(' +
                                json.encode({"signedIn": True}) + ')')
        else:
            self.response.write(callback + '(' +
                                json.encode({"signedIn": False}) + ')')

# Challenging isn't implemented yet.
class Challenge(webapp2.RequestHandler):
    def get(self):
        me = Player.query(
            Player.ID == users.get_current_user()
            ).get()
        them = Player.query(
            Player.ID.nickname() == self.request.get('who')
            ).get()
        if champion:
            challenger.currentChallenge = champion

class QueryChallenger(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        shots = self.request.get('s')
        newPlayer = Player.query(
            Player.ID == users.get_current_user()
            ).get()
        challenger = Player.query(
            Player.ID == newPlayer.currentChallenge
            ).get()
        if(shots):
            if newPlayer.imBoss:
                if newPlayer.onTurn == challenger.onTurn:
                    newPlayer.lastAttack = shots
                    newPlayer.onTurn += 1
                    newPlayer.put()
                    if(newPlayer.onTurn > 3): # 3 means p1 just took their first turn: p2 hasn't moved yet.
                        self.response.write(callback + '(' +
                                            json.encode({"out": challenger.lastAttack}) + ')')
                else:
                    self.response.write(callback + '(' +
                                        json.encode({"out": 'Waiting...'}) + ')')
            else:
                if newPlayer.onTurn < challenger.onTurn:
                    newPlayer.lastAttack = shots
                    newPlayer.onTurn += 1
                    newPlayer.put()
                    self.response.write(callback + '(' +
                                        json.encode({"out": challenger.lastAttack}) + ')')
                else:
                    self.response.write(callback + '(' +
                                        json.encode({"out": 'Waiting...'}) + ')')
        else:
            if challenger:
                self.response.write(callback + '(' + challenger.retrieve() + ')')
            else:
                self.response.write(callback + '(' +
                                    json.encode({"out": 'Waiting...'}) + ')')

# Retrieve the player's account;
# If this fails then something's gone horribly wrong
# (Re)initialise all their ship info
# Push account back to the server
class SetUp(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        newPlayer = Player.query(
            Player.ID == users.get_current_user()
            ).get()
        if newPlayer:
            igun = self.request.get('g')
            isub = self.request.get('s')
            ides = self.request.get('d')
            icru = self.request.get('c')
            icar = self.request.get('a')
            if igun and isub and ides and icru and icar:
                newPlayer.gunboat = igun
                newPlayer.submari = isub
                newPlayer.destroy = ides
                newPlayer.cruiser = icru
                newPlayer.carrier = icar
                newPlayer.onTurn = 1
                newPlayer.lastOn = datetime.datetime.now()
                challenger = Player.query(
                    Player.onTurn == 1).order(Player.lastOn).get()
                if challenger:
                    now = datetime.datetime.utcnow()
                    secs3 = datetime.timedelta(seconds = 3)
                    if(datetime.timedelta(seconds = (now - challenger.lastOn).seconds) > secs3):
                        newPlayer.currentChallenge = challenger.ID
                        newPlayer.onTurn = 2
                        newPlayer.imBoss = False
                        challenger.currentChallenge = newPlayer.ID
                        challenger.onTurn = 2
                        challenger.imBoss = True
                        challenger.put()
                newPlayer.put()
                self.response.write(callback + '(' +
                                    json.encode({"out": 'Success!'}) + ')')
            else:
                self.response.write(callback + '(' +
                                    json.encode({"out": 'Failure!'}) + ')')
        else:
            self.response.write(callback + '(' +
                                json.encode({"out": 'Big failure!'}) + ')')

# onTurn = 0 should mean "player is in lobby"
# It's possible that this could be called too soon after a SetUp(),
# violating the ~1 write/sec rule.
# Some checking may be in order, but it's probably okay.
class Leave(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        newPlayer = Player.query(
            Player.ID == users.get_current_user()
            ).get()
        if newPlayer and newPlayer.onTurn > 0:
            if newPlayer.onTurn > 1:
                challenger = Player.query(
                    Player.ID == newPlayer.currentChallenge
                    ).get()
                challenger.onTurn = 0
                challenger.currentChallenger = None
                challenger.put()
            newPlayer.onTurn = 0
            newPlayer.currentChallenger = None
            newPlayer.put()
            self.response.write(callback + '(' +
                                json.encode({"out": True}) + ')')
        else:
            self.response.write(callback + '(' +
                                json.encode({"out": False}) + ')')

# Differs from Leave in that leave is called when leaving a match,
# and Exit is called when leaving the page
class Exit(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        newPlayer = Player.query(
            Player.ID == users.get_current_user()
            ).get()
        if newPlayer and newPlayer.onTurn > 0:
            if newPlayer.onTurn > 1:
                challenger = Player.query(
                    Player.ID == newPlayer.currentChallenge
                    ).get()
                challenger.onTurn = 0
                challenger.currentChallenge = None
                challenger.put()
            newPlayer.onTurn = -1
            newPlayer.currentChallenge = None
            newPlayer.put()
            self.response.write(callback + '(' +
                                json.encode({"out": True}) + ')')
        else:
            self.response.write(callback + '(' +
                                json.encode({"out": False}) + ')')

class Display(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        callback = self.request.get('callback')
        # This should not be a part of the implementation, as it allows cheating very easily.
        target = self.request.get('who')
        if target:
            targetAcc = Player.query().fetch()
        else:
            playerQuery = Player.query().order(-Player.lastOn)
            iPlayers = playerQuery.fetch(10)
            if iPlayers:
                out = ''
                anyOnline = False
                for player in iPlayers:
                    if(player.onTurn >= 0):
                        anyOnline = True
                        out += '\t' + player.ID.nickname()
                        if(player.onTurn == 0):
                            out += ''
                        else:
                            if(player.onTurn == 1):
                                out += ' is waiting...'
                            else:
                                out += ' is playing'
                        out += '\n'
                if( anyOnline ):
                    self.response.write(callback +'('+
                                        json.encode({"out": out}) +')')
                else:
                    self.response.write(callback +'('+
                                        json.encode({"out": '\tNo players'})+')')
            else:
                self.response.write(callback +'('+
                                    json.encode({"out": '\tNo players'})+')')

class KillAll(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        playerQuery = Player.query().order(-Player.lastOn)
        iPlayers = playerQuery.fetch(10)
        for player in iPlayers:
            player.key.delete()

class Login(webapp2.RequestHandler):
    def get(self):
        self.redirect(users.create_login_url())

app = webapp2.WSGIApplication([
    ('/', SignIn), # Easy manual login check
    ('/enter', AmISignedIn), # Move to game_state
    ('/login', Login), # Necessary for app engine's local network
    ('/leave', Leave), # Move from game_state to lobby_state
    ('/exit', Exit), # Close page
    ('/setup', SetUp), # Post layout
    ('/show', Display), # Show players/player layout
    ('/get', QueryChallenger),
    #('/slap', Challenge),
    ('/final-solution', KillAll)
], debug=True)
