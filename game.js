/**
 * Created by Sean McKay on 09/04/2014.
 */

var game_state = {

    create: function() {
        // Function called after 'preload' to setup the game
        this.music = this.game.add.audio('gameMusic');
        this.music.play("", 0, 0.1, true);
        this.shotAudio = this.game.add.audio('shotAudio');
        this.hitAudio = this.game.add.audio('hitAudio');
        this.missAudio = this.game.add.audio('missAudio');
        this.sinkAudio = this.game.add.audio('sinkAudio');

        this.queryRefresh = this.game.time.now;

        this.board = this.game.add.sprite(0, 400, 'board');
        this.radar = this.game.add.sprite(0, 0, 'radar');
        this.case = this.game.add.sprite(0, 0, 'case');

        this.ships = [new Ship(2, this.game.add.sprite(0, 0, 'boat2'  )),
                      new Ship(3, this.game.add.sprite(0, 0, 'boat3-1')),
                      new Ship(3, this.game.add.sprite(0, 0, 'boat3-2')),
                      new Ship(4, this.game.add.sprite(0, 0, 'boat4'  )),
                      new Ship(5, this.game.add.sprite(0, 0, 'boat5'  ))];
        this.lives = 5;
        this.myShots = [];
        this.hits = [2, 3, 3, 4, 5];
        this.ships[0].setOrigin( 85, 118, false);
        this.ships[1].setOrigin( 85, 217, true );
        this.ships[2].setOrigin(316, 217, true );
        this.ships[3].setOrigin(151, 283, false);
        this.ships[4].setOrigin(184, 118, false);

        this.enemyShips = [new Ship(2, this.game.add.sprite(0, 0, 'boat2')),
                           new Ship(3, this.game.add.sprite(0, 0, 'boat3-1')),
                           new Ship(3, this.game.add.sprite(0, 0, 'boat3-2')),
                           new Ship(4, this.game.add.sprite(0, 0, 'boat4')),
                           new Ship(5, this.game.add.sprite(0, 0, 'boat5'))];
        this.enemyShips[0].image.alpha = 0;
        this.enemyShips[1].image.alpha = 0;
        this.enemyShips[2].image.alpha = 0;
        this.enemyShips[3].image.alpha = 0;
        this.enemyShips[4].image.alpha = 0;
        this.enemyLives = 5;
        this.enemyShots = [];
        this.enemyHits = [2, 3, 3, 4, 5];

        this.hitResult = this.game.add.group();
        this.hitResult.create(-10, -10, 'shot');

        this.shots = this.game.add.group();
        this.shots.createMultiple(200, 'shot');

        this.targets = this.game.add.group();
        this.targets.createMultiple(5, 'targ');

        this.textbox = this.game.add.graphics(0,0);
        this.startGame();

        var style = { font: "27px Arial", fill: "#AAAAAA" };
        this.messageText = this.game.add.text(406, 6, "", style);
        this.messageText.alpha = 0;

        this.switchingView = false;
        this.stateRunning = false;
        this.settingUp = true;
        this.waitingForOpponent = false;

        var enter_key = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter_key.onDown.add(this.switchView, this);
        var s = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        s.onDown.add(this.toggleMusic, this);
        this.game.input.onDown.add(this.clickEvent, this);
        this.game.input.onUp.add(this.declickEvent, this);
    },

    update: function(){
        if( !this.stateRunning ){
            this.moveBox();
        }else if( this.settingUp ){
            this.setupUpdate();
        }else{
            if(this.case.x < 400){
                this.case.x += 10;
            }
            this.checkShots();
            //this.addShot((Math.random()*297) + 52, (Math.random()*297) + 452);

            if(this.queryRefresh+3000 < this.game.time.now) {
                if (this.waitingForOpponent) {
                    this.messageText.content = "";
                    findPlayer(function (response) {
                        if (response.out) {
                            game_state.messageText.content = response.out;
                        } else {
                            game_state.waitingForOpponent = false;
                            game_state.messageText.content = "Connected!";
                            game_state.enemyShips[0].setOriginFromText(response.gunboat);
                            game_state.enemyShips[1].setOriginFromText(response.submari);
                            game_state.enemyShips[2].setOriginFromText(response.destroy);
                            game_state.enemyShips[3].setOriginFromText(response.cruiser);
                            game_state.enemyShips[4].setOriginFromText(response.carrier);
                        }
                    });
                    this.queryRefresh = this.game.time.now;
                }else{
                    findPlayer(function (response) {
                        if (response.out == 'Waiting...') {
                            game_state.messageText.content = 'Waiting...';
                        } else {
                            game_state.messageText.content = response.out;
                            game_state.myShots.pop();
                            game_state.addShot((response.out.charAt(1)*33)+52, (getNum(response.out.charAt(0))*33)+452);
                        }
                    }, this.myShots);
                    this.queryRefresh = this.game.time.now;
                }
            }
        }

        //var blah = "dsa6af";
        //var meh = blah.charAt(3);
        //this.messageText.content = meh;

    },

//---- GAME RULES - ENEMY SHOTS ----//
    explode: function(targ, shot){
        var hit = false;
        shot.x = targ.x;
        shot.y = targ.y;
        var coord = getCoord(shot.x, shot.y);
        var i;
        for(i = 0; i < 5 && !hit; i++){
            hit = this.ships[i].checkShot(coord);
            if( hit ){
                targ.x = targ.x - 10 + (Math.random() * 20);
                targ.y = targ.y - 10 + (Math.random() * 20);
                this.hitResult.create(targ.x, targ.y, 'hita-' + Math.floor(1 + (Math.random()*2.99))).anchor.setTo(0.5,1);
                this.hitAudio.play();
            }
        }
        if( !hit ){
            this.hitResult.create(targ.x, targ.y, 'missa').anchor.setTo(0.5,0.5);
            this.missAudio.play();
        }else{
            if((--this.hits[--i]) == 0){
                this.ships[i].image.alpha = 0.2;
                this.sinkAudio.play("", 0, 0.5);
                if((--this.lives) == 0){
                    this.switchView();
                }
            }
        }
        shot.body.velocity.y = 0;
        shot.alpha = 0;
        targ.kill();
    },
    checkShots: function(){
        this.game.physics.overlap(this.targets, this.shots, this.explode, this.checkOneShot, this);
    },
    checkOneShot: function(targ, shot){
        var dX = targ.x - shot.x;
        var dY = targ.y - shot.y;
        dX = dX*dX;
        dY = dY*dY;
        var dist = dX + dY;
        return dist < 100;
    },
    addShot: function(x, y){
        var mouse = getCoord(x, y);
        x = getX(mouse.charAt(1));
        y = getY(mouse.charAt(0));
        var newTarg = this.targets.getFirstDead();                                                // Make target
        if(newTarg){                                                                              // Check that there was ammo
            newTarg.reset(x, y);                                                                  // Initialise the target
            newTarg.anchor.setTo(0.5, 0.5);
            if(!this.game.physics.overlap(newTarg, this.shots, null, this.checkOneShot, this) &&  // Check the shot hasn't
               !this.game.physics.overlap(newTarg, this.targets, null, this.checkOneShot, this)){ // been done before
                var newShot = this.shots.getFirstDead();
                newShot.reset(x, 435);                                                            // Add the shot
                newShot.body.velocity.y = 66;
                newShot.anchor.setTo(0.5, 0.5);
                //this.shotAudio.play();
            }else{                                                                                // Or
                newTarg.kill();                                                                   // Target wasn't unique:
            }                                                                                     // discard.
        }
    },

    //---- GAME RULES - PLAYER SHOTS ----//
    mark: function(x, y, coord){
        var hit = false;
        var i;
        for(i = 0; i < 5 && !hit; i++){
            hit = this.enemyShips[i].checkUpShot(coord);
            if( hit ){
                this.hitResult.create(x, y, 'hitb').anchor.setTo(0.5,0.5);
            }
        }
        if( !hit ){
            this.hitResult.create(x, y, 'missb').anchor.setTo(0.5,0.5);
        }else{
            if((--this.enemyHits[--i]) == 0){
                this.enemyShips[i].image.alpha = 0.2;
                if((--this.enemyLives) == 0){
                    this.switchView();
                }
            }
        }
    },
    queryTile: function(x, y){
        if(!this.waitingForOpponent && this.myShots.length < 1) {
            if (x < 365 && x > 35 && y < 365 && y > 35) {
                var mouse = getCoord(x, y + 400);
                x = getX(mouse.charAt(1));
                y = getY(mouse.charAt(0)) - 400;
                var newShot = this.shots.getFirstDead();
                if (newShot) {
                    newShot.reset(x, y);
                    newShot.anchor.setTo(0.5, 0.5);
                    newShot.alpha = 0;
                    if (!this.game.physics.overlap(newShot, this.shots, null, this.checkOneShot, this)) {
                        this.mark(x, y, mouse);
                        this.myShots[0] = mouse;
                    } else {
                        newShot.kill();
                    }
                }
            }
        }
    },

    //---- STATE ENTER AND EXIT ----//
    startGame: function(){
        this.textbox.lineStyle(2, 0x000000, 1);
        this.textbox.beginFill(0x555555, 1);
        this.textbox.drawRect(0, 0, 620, 820);
        this.textbox.x = -10;
        this.textbox.y = -10;
    },
    switchView: function() {
        dropOut(
            function(response) {
                if (response.out) {
                    game_state.closeGame();
                }
            });
    },
    moveBox: function() {
        if( this.switchingView ) {
            if (this.textbox.x > -10) {
                this.textbox.x -= 10;
            }else{
                this.closeGame();
                this.game.state.start('lobby');
            }
        }else {
            if (this.textbox.x < 400) {
                this.textbox.x += 10;
            } else {
                this.stateRunning = true;
            }
        }
        var alpha;
        this.messageText.x = this.textbox.x + 6;
        alpha = (this.textbox.x - 200) / 200;
        if(alpha < 0){
            this.messageText.alpha = 0;
        }else{
            this.messageText.alpha = alpha;
        }
    },
    closeGame: function(){
        this.switchingView = true;
        this.stateRunning = false;
        this.music.stop();
    },
    toggleMusic: function() {
        if(this.music.isPlaying){
            this.music.stop();
        }else{
            this.music.play();
        }
    },

    //---- SETUP UI FUNCTIONS ----//
    setupUpdate: function(){
        for(var i = 0; i < 5; i++){
            this.ships[i].drag(game.input.activePointer.x, game.input.activePointer.y);
        }
    },
    setupDeclick: function(){
        var i = 0;
        for (var dropped = false; i < 5 && !dropped; i++) { // Check if the declick resulted in the player dropping a ship.
            dropped = this.ships[i].drop();
        }
        i--;
        if (dropped) {                                      // If a ship was dropped:
            var safe = true;
            for (var j = 0; j < 5; j++) {                   // Check if it was dropped successfully.
                if (j != i) {
                    if (this.ships[i].checkIfCollides(this.ships[j])) {
                        this.ships[i].goToOrigin();
                        safe = false;
                    }
                }
            }
            if (safe) {                                     // If the ship was successfully placed:
                var allDown = true;
                for (var k = 0; k < 5 && allDown; k++) {    // Check if all ships have been placed.
                    allDown = (this.ships[k].getHitbox().length < 4);
                }
                if (allDown) {                              // If all ships have been placed:
                    this.waitingForOpponent = true;
                    postLayout(this.ships, this.moveToPlay);// Push the layout to the server.
                }
            }
        }
    },
    moveToPlay: function(response){
        game_state.settingUp = false;                 // Exit board setup.
        game_state.waitingForOpponent = true;
        game_state.messageText.content = response.out;
    },

    //---- GENERAL MOUSE INPUT ----//
    clickEvent: function() {
        if( this.settingUp ) {
            for(var i = 0; i < 5; i++){
                this.ships[i].checkClick(game.input.activePointer.x, game.input.activePointer.y);
            }
        }else{
            this.queryTile(game.input.activePointer.x, game.input.activePointer.y);
        }
    },
    declickEvent: function() {
        if( this.settingUp ) {
            this.setupDeclick();
        }
    },




    // Restart the game
    restart_game: function() {
        this.closeGame();
        // Start the 'main' state, which restarts the game
        this.game.state.start('game');
    }
};