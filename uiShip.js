/**
 * Created with JetBrains WebStorm.
 * User: b00225409
 * Date: 25/04/14
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */

function Ship(length, image){
    this.x = 0;
    this.y = 0;
    this.originX = 0;
    this.originY = 0;
    this.originV = false;
    this.length = length-1;
    this.vertical = false;
    this.image = image;
    this.image.anchor.setTo(0.5/length, 0.5);
    this.clicked = false;
}

Ship.prototype.checkClick = function(mx, my){
    var dX = Math.abs(this.image.center.x - mx);
    var dY = Math.abs(this.image.center.y - my);
    var width;
    var height;
    if(this.vertical){
        width = this.image.body.height / 2;
        height = this.image.body.width / 2;
    }else{
        width = this.image.body.width / 2;
        height = this.image.body.height / 2;
    }

    if( dX < width && dY < height ){
        this.clicked = true;
        this.x = this.image.x - mx;
        this.y = this.image.y - my;
    }
};

Ship.prototype.drag = function(mx, my){
    if(!this.clicked){
        return;
    }

    if(game.input.keyboard.isDown(Phaser.Keyboard.W)){
        this.vertical = true;
        this.image.anchor.setTo(0.5/(this.length+1), 0.5);
        this.image.angle = 90;
    }else if(game.input.keyboard.isDown(Phaser.Keyboard.A)){
        this.vertical = false;
        this.image.anchor.setTo(0.5/(this.length+1), 0.5);
        this.image.angle = 0;
    }else if(game.input.keyboard.isDown(Phaser.Keyboard.S)){
        this.vertical = true;
        this.image.anchor.setTo(1.0-(0.5/(this.length+1)), 0.5);
        this.image.angle = -90;
    }else if(game.input.keyboard.isDown(Phaser.Keyboard.D)){
        this.vertical = false;
        this.image.anchor.setTo(1.0-(0.5/(this.length+1)), 0.5);
        this.image.angle = 180;
    }

    this.image.x = mx + this.x;
    this.image.y = my + this.y;
};

Ship.prototype.drop = function(){
    if(!this.clicked){
        return false;
    }

    this.clicked = false;
    this.image.x = (Math.round((this.image.x-19)/33)*33)+19;
    this.image.y = (Math.round((this.image.y-23)/33)*33)+23;

    if(this.getHitbox().length > 3){
        this.goToOrigin();
    }

    return true;
};

Ship.prototype.checkIfCollides = function(otherShip){
    var hitbox1 = this.getHitbox();
    var hitbox2 = otherShip.getHitbox();

    if(this.vertical == otherShip.vertical){ // If they are parallel, the test is simpler.
        if(hitbox1.charAt(0) != hitbox2.charAt(0)){ // If they do not lie along the same row/column
            return false;                           // there is no collision.
        }
        if(hitbox1.charAt(1) > hitbox2.charAt(2)){ // If 1's lower bound is larger than 2's upper bound,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(1)){ // If 1's upper bound is less than 2's lower bound,
            return false;                          // there is no collision.
        }
        return true; // If not escaped yet, then they must be collinear and overlapping.
    }else{ // If they are perp, check that 1 is in the rows covered by 2 and 2 is in the cols covered by 1
        if(hitbox1.charAt(1) > hitbox2.charAt(0)){ // If 1's lower bound is larger than 2's row/col,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(0)){ // If 1's upper bound is less than 2's row/col,
            return false;                          // there is no collision.
        }
        if(hitbox2.charAt(1) > hitbox1.charAt(0)){ // If 2's lower bound is larger than 1's row/col,
            return false;                          // there is no collision.
        }
        if(hitbox2.charAt(2) < hitbox1.charAt(0)){ // If 2's upper bound is less than 1's row/col,
            return false;                          // there is no collision.
        }
        return true;
    }
};

Ship.prototype.checkShot = function(shot){
    var hitbox1 = this.getHitbox();
    var hitbox2 = shot;

    if(this.vertical){ // If ship is vertical, begin with col test, else row test
        if(hitbox1.charAt(0) != hitbox2.charAt(1)){ // If they do not lie along the same row/column
            return false;                           // there is no collision.
        }
        if(hitbox1.charAt(1) > hitbox2.charAt(0)){ // If 1's lower bound is larger than 2's upper bound,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(0)){ // If 1's upper bound is less than 2's lower bound,
            return false;                          // there is no collision.
        }
        return true; // If not escaped yet, then shot must lie on ship's col and be within bounds.
    }else{
        if(hitbox1.charAt(0) != hitbox2.charAt(0)){ // If they do not lie along the same row/column
            return false;                           // there is no collision.
        }
        if(hitbox1.charAt(1) > hitbox2.charAt(1)){ // If 1's lower bound is larger than 2's upper bound,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(1)){ // If 1's upper bound is less than 2's lower bound,
            return false;                          // there is no collision.
        }
        return true; // If not escaped yet, then shot must lie on ship's row and be within bounds.
    }
};

Ship.prototype.checkUpShot = function(shot){
    var hitbox1 = this.getUpHitbox();
    var hitbox2 = shot;

    if(this.vertical){ // If ship is vertical, begin with col test, else row test
        if(hitbox1.charAt(0) != hitbox2.charAt(1)){ // If they do not lie along the same row/column
            return false;                           // there is no collision.
        }
        if(hitbox1.charAt(1) > hitbox2.charAt(0)){ // If 1's lower bound is larger than 2's upper bound,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(0)){ // If 1's upper bound is less than 2's lower bound,
            return false;                          // there is no collision.
        }
        return true; // If not escaped yet, then shot must lie on ship's col and be within bounds.
    }else{
        if(hitbox1.charAt(0) != hitbox2.charAt(0)){ // If they do not lie along the same row/column
            return false;                           // there is no collision.
        }
        if(hitbox1.charAt(1) > hitbox2.charAt(1)){ // If 1's lower bound is larger than 2's upper bound,
            return false;                          // there is no collision.
        }
        if(hitbox1.charAt(2) < hitbox2.charAt(1)){ // If 1's upper bound is less than 2's lower bound,
            return false;                          // there is no collision.
        }
        return true; // If not escaped yet, then shot must lie on ship's row and be within bounds.
    }
};

Ship.prototype.getHitbox = function(){
    if(this.vertical){
        return "" + getCol(this.image.x) + getRow(this.image.y) + getRow(this.image.y + (this.length*33));
    }else{
        return "" + getRow(this.image.y) + getCol(this.image.x) + getCol(this.image.x + (this.length*33));
    }
};

Ship.prototype.getUpHitbox = function(){
    if(this.vertical){
        return "" + getCol(this.image.x) + getRow(this.image.y + 400) + getRow(this.image.y + 400 + (this.length*33));
    }else{
        return "" + getRow(this.image.y + 400) + getCol(this.image.x) + getCol(this.image.x + (this.length*33));
    }
};

Ship.prototype.setOriginFromText = function(str){
    if( getNum(str.charAt(0)) === -1 ){
        this.setOrigin((str.charAt(0)*33)+52, (getNum(str.charAt(1))*33)+52, true);
    }else{
        this.setOrigin((str.charAt(1)*33)+52, (getNum(str.charAt(0))*33)+52, false);
    }
};

Ship.prototype.setOrigin = function(x, y, v){
    this.originX = x;
    this.originY = y;
    this.originV = v;
    this.goToOrigin();
};

Ship.prototype.goToOrigin = function(){
    this.image.x = this.originX;
    this.image.y = this.originY;
    this.vertical = this.originV;
    if(this.vertical){
        this.image.anchor.setTo(0.5/(this.length+1), 0.5);
        this.image.angle = 90;
    }else{
        this.image.anchor.setTo(0.5/(this.length+1), 0.5);
        this.image.angle = 0;
    }
};