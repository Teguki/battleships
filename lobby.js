/**
 * Created by Sean McKay on 09/04/2014.
 */

var lobby_state = {

    create: function() {
        // Function called after 'preload' to setup the game
        this.music = this.game.add.audio('lobbyMusic');
        this.music.play("", 0, 0.025, true);
        //this.music.play();

        var style = { font: "27px Arial", fill: "#ffffff" };
        this.users = this.game.add.text(6, 6, "", style);   //usernames from above passed in and displayed
        this.users.alpha = 0;
        getPlayers(this.loadPlayers, this.failedToLoadPlayers);
        this.textRefresh = this.game.time.now;


        this.textbox = this.game.add.graphics(0,0);
        this.startGame();

        this.chat = this.game.add.text(6, 6, "", style);
        this.chat.alpha = 0
        this.chat.content = "Chat/manual challenges aren't done yet;\n\tPress enter to play against a random player.";

        this.switchingView = false;
        this.stateRunning = false;

        var enter_key = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter_key.onDown.add(this.switchView, this);
        var s = this.game.input.keyboard.addKey(Phaser.Keyboard.S);
        s.onDown.add(this.toggleMusic, this);
    },

    update: function() {
        if( !this.stateRunning ){
            this.moveBox();
        }else{
            if(this.textRefresh+1000 < this.game.time.now){
                getPlayers(this.loadPlayers, this.failedToLoadPlayers);
                this.textRefresh = this.game.time.now;
            }
        }
    },

    //---- STATE ENTER AND EXIT ----//
    loadPlayers: function(players){
        lobby_state.users.content = "Players Connected:\n" + players.out;
    },
    failedToLoadPlayers: function(){
        lobby_state.users.content = "Players Connected:\n\nError:\nThe server had a problem.";
    },
    startGame: function(){
        this.textbox.lineStyle(2, 0x000000, 1);
        this.textbox.beginFill(0x555555, 1);
        this.textbox.drawRect(0, 0, 620, 820);
        this.textbox.x = -10;
        this.textbox.y = -10;
    },
    switchView: function() {
        checkLoginStatus(
            function(response){
                if(response.signedIn){
                    lobby_state.closeGame();
                }else{
                    lobby_state.chat.content = "You don't appear to be signed in!（ﾟロ ﾟ ' ）\n\n\tPlease sign into your Google account to play.\n\t\tggbattleships.appspot.com/login\n\t\t\t(You will not need to refresh this page.)";
                }
            });
    },
    moveBox: function() {
        if (this.switchingView) {
            if (this.textbox.y > -10) {
                this.textbox.y -= 10;
            } else {
                this.closeGame();
                this.game.state.start('game');
            }
        } else {
            if (this.textbox.y < 400) {
                this.textbox.y += 10;
            } else {
                this.stateRunning = true
            }
        }
        var alpha;
        this.users.y = 406 - this.textbox.y;
        alpha = (this.textbox.y - 200) / 200;
        if(alpha < 0){
            this.users.alpha = 0;
        }else{
            this.users.alpha = alpha;
        }
        this.chat.y = this.textbox.y + 6;
        alpha = (this.textbox.y - 200) / 200;
        if(alpha < 0){
            this.chat.alpha = 0;
        }else{
            this.chat.alpha = alpha;
        }
    },
    closeGame: function(){
        this.switchingView = true;
        this.stateRunning = false;
        this.game.time.events.remove(this.textRefresh);
        this.music.stop();
    },
    toggleMusic: function() {
        if(this.music.isPlaying){
            this.music.stop();
        }else{
            this.music.play();
        }
    },

    // Restart the game
    restart_game: function() {
        this.closeGame();
        // Start the 'main' state, which restarts the game
        this.game.state.start('lobby');
    }

};