/**
 * Created by Sean McKay on 30/04/2014.
 */

var itoc = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

getChar = function(number){
    number = Math.round(number);
    if(number > -1 && number < 10){
        return itoc[number];
    }
    return -1;
};

getNum = function(letter){
    for(var i = 0; i < 10; i++){
        if(letter == itoc[i]){
            return i;
        }
    }
    return -1;
};

getCol = function(x){
    return "" + Math.round((x-52)/33);
};

getRow = function(y){
    return "" + getChar((y-452)/33);
};

getX = function(row){
    return (row*33)+52;
};

getY = function(col){
    col = getNum(col);
    return (col*33)+452;
};

getCoord = function(x, y) {
    return "" + getRow(y) + getCol(x);
};