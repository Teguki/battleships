/**
 * Created by Sean McKay on 03/05/2014.
 */

var titles = [
    'Battleships',
    'The Little Battleship that Could',	// The Little Engine that Could
    'A Tale of Ten Battleships',		// A Tale of Two Cities
    'Battleship Down'					// Watership Down
];
var title = titles[parseInt(Math.random() * titles.length)];
document.write('<title>' + title + '</title>');