// Initialize Phaser, and creates a 400x490px game
var game = new Phaser.Game(600, 800, Phaser.AUTO, 'game_div');

game.state.add('load', load_state);
game.state.add('lobby', lobby_state);
game.state.add('game', game_state);

game.state.start('load');

window.addEventListener("beforeunload", function(event){
    closingWindow();
    (event || window.event).returnValue = "Wait! You may still be in a match!";
});